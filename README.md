# Zhivotot e krug! 📱
Welcome to Zhivotot e krug! Zhivotot e krug is a small android application developed for my Android class. Made with Java and Firebase. Zhivotot e krug uses Firebase Authentication for login and authentication of users, CRUD operations are done with Firebase Realtime Database. ✨


# How to use this? 🤔
If your want to try this app on your android phone, follow these steps: 
1. Clone this repository in your local environment by the following command:
```git clone https://gitlab.com/natalijastojanovska/zhivotot-e-krug.git```

2. Create your own Firebase project <br>
```https://console.firebase.google.com/```

3. On your Firebase project add sign-in providers: <br>
```Email/Password```

4. Download ```google-services.json``` and place it in the app/directory (at the root of the Android Studio app module).

5. Activate developer options on your android device, connect it to your PC and run the project. It will take few minutes to install.

# How it works? 🛠
You can register as a Volunteer or an Adult. If you chose Adult role, after signing in you will see Adult Activity where are listed all tasks created by you. If you click on a plus button, Create New Task Activity will open where you can create tasks, after filling the name, description, time and location of that particular task. When your task is created it will appear on the Adult Activity. If your role is Volunteer, when you open the app the Volunteer Activity will open and there will be listed all the tasks that you had applied for. If you click on a plus button, Add New Task Activity will open and list of all pending tasks, that you haven't apply for will appear. 

# Inspiration 💡
Workshop project for my Android Programming Course

