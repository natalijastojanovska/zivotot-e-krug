package com.example.zhivototekrug;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdultActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {

    MyRecyclerViewAdapter adapter;
    FirebaseDatabase database;
    DatabaseReference db;
    List<TaskInfo> pom = new ArrayList<>();
    float x1, x2, y1, y2;
    String vIme, vPrezime, vMail, vBroj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adult);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regIntent = new Intent(AdultActivity.this, NewTaskActivity.class);
                startActivity(regIntent);
            }
        });



        RecyclerView recyclerView = findViewById(R.id.rvTasks);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        db = database.getInstance().getReference();
        db.child("tasks").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                for (DataSnapshot objSnapshot : snapshot.getChildren()) {
                    TaskInfo data = new TaskInfo(objSnapshot.child("task").getValue().toString(),
                            objSnapshot.child("description").getValue().toString(),
                            objSnapshot.child("date").getValue().toString(),
                            objSnapshot.child("frequency").getValue().toString(),
                            objSnapshot.child("location").getValue().toString(),
                            FirebaseAuth.getInstance().getUid(),
                            objSnapshot.child("adlName").getValue().toString(),
                            objSnapshot.child("adlMail").getValue().toString(),
                            objSnapshot.child("adlNum").getValue().toString());
                    data.setState(objSnapshot.child("state").getValue().toString());
                    data.setAdlReview(objSnapshot.child("adlReview").getValue().toString());
                    data.setTempVol(objSnapshot.child("tempVol").getValue().toString());
                    data.setVolMail(objSnapshot.child("volMail").getValue().toString());
                    data.setVolName(objSnapshot.child("volName").getValue().toString());
                    data.setVolNum(objSnapshot.child("volNum").getValue().toString());
                    data.setVolReview(objSnapshot.child("volReview").getValue().toString());
                    data.setVolunteer(objSnapshot.child("volunteer").getValue().toString());

                    pom.add(data);
                }

                adapter = new MyRecyclerViewAdapter(AdultActivity.this, pom);
                adapter.setClickListener(AdultActivity.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });


    }

    @Override
    public void onItemClick(View view, int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(AdultActivity.this, R.style.AlertDialogStyle);
        builder1.setTitle(pom.get(position).getTask());
        if(pom.get(position).getTempVol().equals("/")){
            builder1.setMessage(" Description: " + pom.get(position).getDescription()
                    + "\n Location: " + pom.get(position).getLocation()
                    + "\n Frequency: " + pom.get(position).getFrequency()
                    + "\n Status: " + pom.get(position).getState());
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Okay",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }else if(!pom.get(position).getTempVol().equals("/")){
            if(pom.get(position).getState().equals("pending")){
                db.child("users").child(pom.get(position).getTempVol()).addValueEventListener(new ValueEventListener(){
                    @Override
                    public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                        vMail = snapshot.child("email").getValue().toString();
                        vIme = snapshot.child("firstName").getValue().toString();
                        vPrezime = snapshot.child("lastName").getValue().toString();
                        vBroj = snapshot.child("number").getValue().toString();
                    }

                    @Override
                    public void onCancelled(@NonNull @NotNull DatabaseError error) {

                    }
                                                        });
                builder1.setMessage(" Description: " + pom.get(position).getDescription()
                        + "\n Location: " + pom.get(position).getLocation()
                        + "\n Frequency: " + pom.get(position).getFrequency()
                        + "\n Status: " + pom.get(position).getState()
                        + "\n Volunteer: " + vIme + " " + vPrezime
                        + "\n Number: " + vBroj
                        + "\n Email: " + vMail);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Accept",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                TaskInfo update = new TaskInfo(pom.get(position).getTask(), pom.get(position).getDescription(), pom.get(position).getDate(), pom.get(position).getFrequency(), pom.get(position).getLocation(), pom.get(position).getAdult(), pom.get(position).getAdlName(), pom.get(position).getAdlMail(), pom.get(position).getAdlNum());
                                update.setVolunteer(pom.get(position).getTempVol());
                                update.setVolNum(vBroj);
                                update.setVolName(vIme + " " + vPrezime);
                                update.setVolMail(vMail);
                                update.setTempVol(pom.get(position).getTempVol());
                                update.setState("active");
                                db.child("tasks").child(update.getTask()).setValue(update);
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "Reject",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                TaskInfo update = new TaskInfo(pom.get(position).getTask(), pom.get(position).getDescription(), pom.get(position).getDate(), pom.get(position).getFrequency(), pom.get(position).getLocation(), pom.get(position).getAdult(), pom.get(position).getAdlName(), pom.get(position).getAdlMail(), pom.get(position).getAdlNum());
                                update.setVolunteer("/");
                                dialog.cancel();
                            }
                        });

            }else if(pom.get(position).getState().equals("active")){
                builder1.setMessage(" Description: " + pom.get(position).getDescription()
                        + "\n Location: " + pom.get(position).getLocation()
                        + "\n Frequency: " + pom.get(position).getFrequency()
                        + "\n Status: " + pom.get(position).getState()
                        + "\n Volunteer: " + pom.get(position).getVolName()
                        + "\n Number: " + pom.get(position).getVolNum()
                        + "\n Email: " + pom.get(position).getVolMail());
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Okay",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
            }else if(pom.get(position).getState().equals("finished")){
                if(pom.get(position).getAdlReview().equals("/")){
                    String[] items = {"1", "2", "3", "4", "5"};
                    int checkedItem = 2;
                    builder1.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    pom.get(position).setAdlReview("1");
                                    db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                    break;
                                case 1:
                                    pom.get(position).setAdlReview("2");
                                    db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                    break;
                                case 2:
                                    pom.get(position).setAdlReview("3");
                                    db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                    break;
                                case 3:
                                    pom.get(position).setAdlReview("4");
                                    db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                    break;
                                case 4:
                                    pom.get(position).setAdlReview("5");
                                    db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                    break;
                            }
                        }
                });

        }else{ builder1.setMessage(" Description: " + pom.get(position).getDescription()
                        + "\n Location: " + pom.get(position).getLocation()
                        + "\n Frequency: " + pom.get(position).getFrequency()
                        + "\n Status: " + pom.get(position).getState()
                        + "\n Volunteer: " + pom.get(position).getVolName()
                        + "\n Number: " + pom.get(position).getVolNum()
                        + "\n Email: " + pom.get(position).getVolMail()
                        + "\n Review: " + pom.get(position).getAdlReview());
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Okay",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                }
            }}

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public boolean onTouchEvent(MotionEvent touchEvent){
        switch(touchEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();
                if(x1<x2){
                    Intent i = new Intent(AdultActivity.this, InfoActivity.class);
                    startActivity(i);
                }
                break;
        }
        return false;
    }

}