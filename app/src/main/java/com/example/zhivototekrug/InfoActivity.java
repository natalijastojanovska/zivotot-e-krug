package com.example.zhivototekrug;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zhivototekrug.databinding.ActivityInfoBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

public class InfoActivity extends AppCompatActivity {

    private ActivityInfoBinding binding;
    private FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference db;
    String[] info = new String[8];
    int p = 0;
    int f = 0;
    int a = 0;
    Infos infos;
    float volRate = 0;
    float adlRate = 0;
    int volBr = 0;
    int adlBr = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        auth = FirebaseAuth.getInstance();
        binding = ActivityInfoBinding.inflate(getLayoutInflater());
        Button signOut = binding.signOut;
        TextView type = binding.type;
        TextView name = binding.name;
        TextView email = binding.email;
        TextView number = binding.number;
        TextView pending = binding.pending;
        TextView active = binding.active;
        TextView finished = binding.finished;

        db = database.getInstance().getReference();
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                info[0] = snapshot.child("users").child(auth.getCurrentUser().getUid()).child("userType").getValue().toString();
                info[1] = snapshot.child("users").child(auth.getCurrentUser().getUid()).child("firstName").getValue().toString();
                info[2] = snapshot.child("users").child(auth.getCurrentUser().getUid()).child("lastName").getValue().toString();
                info[3] = snapshot.child("users").child(auth.getCurrentUser().getUid()).child("number").getValue().toString();
                info[4] = snapshot.child("users").child(auth.getCurrentUser().getUid()).child("email").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
        db.child("tasks").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot objSnapshot : snapshot.getChildren()) {
                    if(objSnapshot.child("adult").getValue().toString().equals(auth.getCurrentUser().getUid())){
                        if (!objSnapshot.child("volReview").getValue().toString().equals("/")){
                            adlBr++;
                            adlRate = Float.parseFloat(objSnapshot.child("volReview").getValue().toString());
                        }else if (objSnapshot.child("state").getValue().toString().equals("pending")){
                            p++;
                        }else if (objSnapshot.child("state").getValue().toString().equals("active")){
                            a++;
                        }else if (objSnapshot.child("state").getValue().toString().equals("finished")){
                            f++;
                        }
                    }else if(objSnapshot.child("tempVol").getValue().toString().equals(auth.getCurrentUser().getUid())) {
                        if (!objSnapshot.child("adlReview").getValue().toString().equals("/")){
                            volBr++;
                            volRate = Float.parseFloat(objSnapshot.child("adlReview").getValue().toString());
                        }else if (objSnapshot.child("state").getValue().toString().equals("active")) {
                            a++;
                        } else if (objSnapshot.child("state").getValue().toString().equals("finished")) {
                            f++;
                        } else if (objSnapshot.child("state").getValue().toString().equals("pending")) {
                            p++;
                        }
                    }
                }
                adlRate = adlRate/adlBr;
                volRate = volRate/volBr;
                info[5] = String.valueOf(p);
                info[6] = String.valueOf(a);
                info[7] = String.valueOf(f);
                if(info[0].equals("Volunteer")){
                    infos = new Infos(info[0], info[1] + " " + info[2], info[3], info[4], info[5], info[6], info[7], Float.toString(volRate));
                }else{
                    infos = new Infos(info[0], info[1] + " " + info[2], info[3], info[4], info[5], info[6], info[7], Float.toString(adlRate));
                }

                binding.setInfos(infos);
                binding.executePendingBindings();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });

        setContentView(binding.getRoot());




        View.OnClickListener btnClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regIntent = new Intent(InfoActivity.this, LoginActivity.class);
                auth.signOut();
                startActivity(regIntent);
            }
        };
        signOut.setOnClickListener(btnClick);


    }
}