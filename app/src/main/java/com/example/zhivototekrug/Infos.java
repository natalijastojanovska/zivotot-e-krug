package com.example.zhivototekrug;

public class Infos {
    private String name;
    private String number;
    private String userType;
    private String email;
    private String pending;
    private String active;
    private String finished;
    private String rating;

    public Infos() {

    }

    public Infos(String type, String name, String number, String email, String pending, String active, String finished, String rating){
        this.active = active;
        this.email = email;
        this.finished = finished;
        this.name = name;
        this.number = number;
        this.userType = type;
        this.pending = pending;
        this.rating = rating;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getFinished() {
        return finished;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
