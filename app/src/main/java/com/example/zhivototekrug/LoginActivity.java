package com.example.zhivototekrug;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zhivototekrug.databinding.ActivityLoginBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

public class LoginActivity extends AppCompatActivity {


    private ActivityLoginBinding binding;
    private FirebaseAuth auth;
    private static final String TAG = LoginActivity.class.getSimpleName();
    FirebaseDatabase database;
    DatabaseReference db;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();
        auth.signOut();

        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        TextInputLayout username = binding.username;
        TextInputLayout password = binding.password;
        Button login = binding.buttonSignIn;
        Button register = binding.signUp;
        setContentView(binding.getRoot());



        View.OnClickListener btnClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (username.getEditText().getText().toString().contains("@")) {
                    if (password.getEditText().getText().length() > 5) {
                        auth.signInWithEmailAndPassword(username.getEditText().getText().toString(), password.getEditText().getText().toString())
                                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            Log.d(TAG, "signInWithEmail:success");


                                            db = database.getInstance().getReference();
                                            db.addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                                                    updateUI(snapshot.child("users").child(auth.getCurrentUser().getUid()).child("userType").getValue().toString());
                                                }

                                                @Override
                                                public void onCancelled(@NonNull @NotNull DatabaseError error) {
                                                    Toast.makeText(LoginActivity.this, "ne mozhe da zeme podatoci od data bazata", Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                        } else {
                                            // If sign in fails, display a message to the user.
                                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                                            Toast.makeText(getBaseContext(), "Authentication failed.",
                                                    Toast.LENGTH_SHORT).show();
                                            //updateUI(null);
                                        }
                                    }
                                });
                    }
                }
            }

            private void updateUI(String type) {
                Intent activityIntent;

                if(type.equals("Adult")){
                    activityIntent = new Intent(LoginActivity.this, AdultActivity.class);
                }else if(type.equals("Volunteer")){
                    activityIntent = new Intent(LoginActivity.this, VolunteerActivity.class);
                }else{
                    activityIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                }

                startActivity(activityIntent);
            }
        };
        login.setOnClickListener(btnClick);


        View.OnClickListener btnReg = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(regIntent);
            }
        };
        register.setOnClickListener(btnReg);


    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser currentUser = auth.getCurrentUser();

        if (currentUser != null) {
            auth.signOut();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null) {
            auth.signOut();
        }
    }

}