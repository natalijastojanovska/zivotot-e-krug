package com.example.zhivototekrug;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zhivototekrug.databinding.ActivityNewTaskBinding;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class NewTaskActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private @NonNull ActivityNewTaskBinding binding;
    final Calendar myCalendar= Calendar.getInstance();
    TextView textView;

    FirebaseDatabase database;
    DatabaseReference db;
    TaskInfo taskInfo;
    String adlName, adlNumber, adlEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();
        binding = ActivityNewTaskBinding.inflate(getLayoutInflater());
        TextInputLayout task = binding.task;
        TextInputLayout desc = binding.description;
        TextView datePicker = binding.datePicker;
        TextInputLayout location = binding.location;
        RadioGroup radio = binding.radio;
        Button create = binding.create;


        setContentView(binding.getRoot());

        db =database.getInstance().getReference();
        db.child("users").child(auth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                adlEmail = snapshot.child("email").getValue().toString();
                adlName = snapshot.child("firstName").getValue().toString() + " " + snapshot.child("lastName").getValue().toString();
                adlNumber = snapshot.child("number").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
        taskInfo = new TaskInfo();

        create.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(task.getEditText().getText().toString().isEmpty()){
                    Toast.makeText(NewTaskActivity.this, "Enter task", Toast.LENGTH_LONG).show();
                }else if(desc.getEditText().getText().toString().isEmpty()){
                    Toast.makeText(NewTaskActivity.this, "Description is missing", Toast.LENGTH_LONG).show();
                }else if(location.getEditText().getText().toString().isEmpty()){
                    Toast.makeText(NewTaskActivity.this, "Pick a location", Toast.LENGTH_LONG).show();
                }else if(datePicker.getText().equals("dd/mm/yyyy")){
                    Toast.makeText(NewTaskActivity.this, "Invalid or no date", Toast.LENGTH_LONG).show();
                }else{
                    RadioButton checked = findViewById(radio.getCheckedRadioButtonId());
                    addDataToFirebase(task.getEditText().getText().toString(), desc.getEditText().getText().toString(), datePicker.getText().toString(), location.getEditText().getText().toString(), checked.getText().toString());

                    Intent regIntent = new Intent(NewTaskActivity.this, AdultActivity.class);
                    startActivity(regIntent);
                }
            }
        });

        textView=(TextView) findViewById(R.id.datePicker);
        DatePickerDialog.OnDateSetListener date =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH,month);
                myCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateLabel();
            }
        };
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(NewTaskActivity.this,date,myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void updateLabel(){
        String myFormat="dd/MM/yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.US);
        textView.setText(dateFormat.format(myCalendar.getTime()));
    }
    private void addDataToFirebase(String task, String desc, String date, String location, String freq) {
        taskInfo = new TaskInfo(task, desc, date, freq, location, auth.getCurrentUser().getUid(), adlName, adlEmail, adlNumber);

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                db.child("tasks").child(task).setValue(taskInfo);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });


    }
}