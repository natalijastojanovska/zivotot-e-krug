package com.example.zhivototekrug;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.zhivototekrug.databinding.ActivityRegisterBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private ActivityRegisterBinding binding;

    FirebaseDatabase database;
    DatabaseReference db;
    UserInfo userInfo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        TextInputLayout email = binding.username;
        TextInputLayout fstName = binding.fistName;
        TextInputLayout lstName = binding.lastName;
        TextInputLayout number = binding.number;
        TextInputLayout password = binding.password;
        TextInputLayout rtPassword = binding.repeatPassword;
        Button registerButton = binding.register;
        Button signInButton = binding.signIn;
        RadioGroup group = binding.radio;

        setContentView(binding.getRoot());

        db =database.getInstance().getReference();
        userInfo = new UserInfo();

        View.OnClickListener btnSignIn = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getEditText().getText().toString().isEmpty() || !email.getEditText().getText().toString().contains("@")) {
                    Toast.makeText(RegisterActivity.this, "Wrong input for E-mail", Toast.LENGTH_LONG).show();
                } else if (password.getEditText().getText().toString().length() < 5) {
                    Toast.makeText(RegisterActivity.this, "Too short", Toast.LENGTH_LONG).show();
                } else if (!rtPassword.getEditText().getText().toString().equals(password.getEditText().getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "Passwords don't match", Toast.LENGTH_LONG).show();
                } else if (fstName.getEditText().getText().toString().isEmpty()) {
                    Toast.makeText(RegisterActivity.this, "Enter your First name", Toast.LENGTH_LONG).show();
                } else if (lstName.getEditText().getText().toString().isEmpty()) {
                    Toast.makeText(RegisterActivity.this, "Enter your Last name", Toast.LENGTH_LONG).show();
                } else if (number.getEditText().getText().toString().isEmpty() || !number.getEditText().getText().toString().startsWith("07")) {
                    Toast.makeText(RegisterActivity.this, "Wrong input for your number", Toast.LENGTH_LONG).show();
                } else {
                    auth.createUserWithEmailAndPassword(email.getEditText().getText().toString(), password.getEditText().getText().toString())
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        FirebaseUser user = auth.getCurrentUser();
                                        RadioButton adult = findViewById(group.getCheckedRadioButtonId());
                                        addDataToFirebase(user, fstName.getEditText().getText().toString(), lstName.getEditText().getText().toString(), number.getEditText().getText().toString(), adult.getText().toString());
                                        Log.d(TAG, "createUserWithEmail:success");
                                        finish();
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                        Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            });
                }
            }

        };
        registerButton.setOnClickListener(btnSignIn);

        View.OnClickListener btnReg = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent regIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(regIntent);
            }
        };
        signInButton.setOnClickListener(btnReg);
    }
    private void addDataToFirebase(FirebaseUser user,String firstName, String lastName, String number, String userType) {
        userInfo.setFirstName(firstName);
        userInfo.setLastName(lastName);
        userInfo.setNumber(number);
        userInfo.setUserType(userType);
        userInfo.setEmail(user.getEmail());

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                db.child("users").child(user.getUid()).setValue(userInfo);
                Toast.makeText(RegisterActivity.this, "data added", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(RegisterActivity.this, "Fail to add data " + error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
