package com.example.zhivototekrug;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SelectTaskActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener{

    MyRecyclerViewAdapter adapter;
    private FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference db;
    List<TaskInfo> pom = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_task);
        auth = FirebaseAuth.getInstance();

      RecyclerView recyclerView = findViewById(R.id.rvTasks);
      recyclerView.setLayoutManager(new LinearLayoutManager(this));

        db = database.getInstance().getReference();
        db.child("tasks").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot objSnapshot : snapshot.getChildren()) {
                    if(objSnapshot.child("tempVol").getValue().equals("/")){
                        TaskInfo data = new TaskInfo(objSnapshot.child("task").getValue().toString(),
                                objSnapshot.child("description").getValue().toString(),
                                objSnapshot.child("date").getValue().toString(),
                                objSnapshot.child("frequency").getValue().toString(),
                                objSnapshot.child("location").getValue().toString(),
                                objSnapshot.child("adult").getValue().toString(),
                                objSnapshot.child("adlName").getValue().toString(),
                                objSnapshot.child("adlMail").getValue().toString(),
                                objSnapshot.child("adlNum").getValue().toString());
                        data.setState(objSnapshot.child("state").getValue().toString());
                        data.setAdlReview(objSnapshot.child("adlReview").getValue().toString());
                        data.setTempVol(objSnapshot.child("tempVol").getValue().toString());
                        data.setVolMail(objSnapshot.child("volMail").getValue().toString());
                        data.setVolName(objSnapshot.child("volName").getValue().toString());
                        data.setVolNum(objSnapshot.child("volNum").getValue().toString());
                        data.setVolReview(objSnapshot.child("volReview").getValue().toString());
                        data.setVolunteer(objSnapshot.child("volunteer").getValue().toString());

                        pom.add(data);
                    }
                }


                adapter = new MyRecyclerViewAdapter(SelectTaskActivity.this, pom);
                adapter.setClickListener(SelectTaskActivity.this);
                recyclerView.setAdapter(adapter);

            }


            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });



    }

    @Override
    public void onItemClick(View view, int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(SelectTaskActivity.this, R.style.AlertDialogStyle);
        builder1.setTitle(pom.get(position).getTask());
        builder1.setMessage(" Description: " + pom.get(position).getDescription()
                + "\n Location: " + pom.get(position).getLocation()
                + "\n Frequency: " + pom.get(position).getFrequency()
                + "\n Status: " + pom.get(position).getState()
                + "\n Adult: " + pom.get(position).getAdlName()
                + "\n Number: " + pom.get(position).getAdlNum()
                + "\n Email: " + pom.get(position).getAdlMail());
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Apply",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        TaskInfo update = new TaskInfo(pom.get(position).getTask(), pom.get(position).getDescription(), pom.get(position).getDate(), pom.get(position).getFrequency(), pom.get(position).getLocation(), pom.get(position).getAdult(), pom.get(position).getAdlName(), pom.get(position).getAdlMail(), pom.get(position).getAdlNum());
                        update.setTempVol(auth.getCurrentUser().getUid());
                        update.setState("pending");
                        db.child("tasks").child(update.getTask()).setValue(update);
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}