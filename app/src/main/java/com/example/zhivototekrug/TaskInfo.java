package com.example.zhivototekrug;

public class TaskInfo {

    public String task;
    public String description;
    public String date;
    public String frequency;
    public String location;
    public String state;
    public String adult;
    public String adlName;
    public String adlNum;
    public String adlMail;
    public String adlReview;
    public String volunteer;
    public String tempVol;
    public String volName;
    public String volNum;
    public String volMail;
    public String volReview;

    public TaskInfo() {

    }

    public TaskInfo(String task, String desc, String date, String freq, String loc, String adl, String adlName, String adlMail, String adlNum){
        this.task=task;
        this.description=desc;
        this.date = date;
        this.frequency = freq;
        this.location = loc;
        this.adult = adl;
        this.state = "pending";
        this.adlName = adlName;
        this.adlMail = adlMail;
        this.adlNum = adlNum;
        this.adlReview = "/";
        this.volName = "/";
        this.volMail = "/";
        this.volNum = "/";
        this.volReview = "/";
        this.tempVol = "/";
        this.volunteer = "/";
    }


    public String getAdult() {
        return adult;
    }

    public void setAdult(String adult) {
        this.adult = adult;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getFrequency() {
        return frequency;
    }
    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public String getTask() {
        return task;
    }
    public void setTask(String task) {
        this.task = task;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAdlName() {
        return adlName;
    }

    public void setAdlName(String adlName) {
        this.adlName = adlName;
    }

    public String getAdlNum() {
        return adlNum;
    }

    public void setAdlNum(String adlNum) {
        this.adlNum = adlNum;
    }

    public String getAdlMail() {
        return adlMail;
    }

    public void setAdlMail(String adlMail) {
        this.adlMail = adlMail;
    }

    public String getAdlReview() {
        return adlReview;
    }

    public void setAdlReview(String adlReview) {
        this.adlReview = adlReview;
    }

    public String getVolunteer() {
        return volunteer;
    }

    public void setVolunteer(String volunteer) {
        this.volunteer = volunteer;
    }

    public String getTempVol() {
        return tempVol;
    }

    public void setTempVol(String tempVol) {
        this.tempVol = tempVol;
    }

    public String getVolName() {
        return volName;
    }

    public void setVolName(String volName) {
        this.volName = volName;
    }

    public String getVolNum() {
        return volNum;
    }

    public void setVolNum(String volNum) {
        this.volNum = volNum;
    }

    public String getVolMail() {
        return volMail;
    }

    public void setVolMail(String volMail) {
        this.volMail = volMail;
    }

    public String getVolReview() {
        return volReview;
    }

    public void setVolReview(String volReview) {
        this.volReview = volReview;
    }

}

