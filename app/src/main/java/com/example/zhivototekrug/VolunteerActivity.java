package com.example.zhivototekrug;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class VolunteerActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {

    MyRecyclerViewAdapter adapter;
    FirebaseDatabase database;
    DatabaseReference db;
    List<TaskInfo> pom = new ArrayList<>();
    float x1, x2, y1, y2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volunteer);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regIntent = new Intent(VolunteerActivity.this, SelectTaskActivity.class);
                startActivity(regIntent);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rvTasks);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        db = database.getInstance().getReference();
        db.child("tasks").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                for (DataSnapshot objSnapshot : snapshot.getChildren()) {
                        TaskInfo data = new TaskInfo(objSnapshot.child("task").getValue().toString(),
                                objSnapshot.child("description").getValue().toString(),
                                objSnapshot.child("date").getValue().toString(),
                                objSnapshot.child("frequency").getValue().toString(),
                                objSnapshot.child("location").getValue().toString(),
                                objSnapshot.child("adult").getValue().toString(),
                                objSnapshot.child("adlName").getValue().toString(),
                                objSnapshot.child("adlMail").getValue().toString(),
                                objSnapshot.child("adlNum").getValue().toString());
                        data.setState(objSnapshot.child("state").getValue().toString());
                        data.setAdlReview(objSnapshot.child("adlReview").getValue().toString());
                        data.setTempVol(objSnapshot.child("tempVol").getValue().toString());
                        data.setVolMail(objSnapshot.child("volMail").getValue().toString());
                        data.setVolName(objSnapshot.child("volName").getValue().toString());
                        data.setVolNum(objSnapshot.child("volNum").getValue().toString());
                        data.setVolReview(objSnapshot.child("volReview").getValue().toString());
                        data.setVolunteer(objSnapshot.child("volunteer").getValue().toString());

                        if(!data.getTempVol().equals("/")){
                            pom.add(data);
                        }

                }

                adapter = new MyRecyclerViewAdapter(VolunteerActivity.this, pom);
                adapter.setClickListener(VolunteerActivity.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });


    }

    @Override
    public void onItemClick(View view, int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(VolunteerActivity.this, R.style.AlertDialogStyle);
        builder1.setTitle(pom.get(position).getTask());
        if(pom.get(position).getState().equals("pending")){

                builder1.setMessage(" Description: " + pom.get(position).getDescription()
                        + "\n Location: " + pom.get(position).getLocation()
                        + "\n Frequency: " + pom.get(position).getFrequency()
                        + "\n Status: " + pom.get(position).getState()
                        + "\n Adult: " + pom.get(position).getAdlName()
                        + "\n Number: " + pom.get(position).getAdlNum()
                        + "\n Email: " + pom.get(position).getAdlMail());
                builder1.setCancelable(true);


                builder1.setPositiveButton(
                        "Okay",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
            }else if(pom.get(position).getState().equals("active")){
            builder1.setMessage(" Description: " + pom.get(position).getDescription()
                    + "\n Location: " + pom.get(position).getLocation()
                    + "\n Frequency: " + pom.get(position).getFrequency()
                    + "\n Status: " + pom.get(position).getState()
                    + "\n Adult: " + pom.get(position).getAdlName()
                    + "\n Number: " + pom.get(position).getAdlNum()
                    + "\n Email: " + pom.get(position).getAdlMail());
            builder1.setCancelable(true);


            builder1.setPositiveButton(
                    "Finish",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            pom.get(position).setState("finished");
                            db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                            dialog.cancel();
                        }
                    });
        }else if(pom.get(position).getState().equals("finished")){
            if(pom.get(position).getVolReview().equals("/")){
                String[] items = {"1", "2", "3", "4", "5"};
                int checkedItem = 2;
                builder1.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        pom.get(position).setVolReview("1");
                                        db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                        break;
                                    case 1:
                                        pom.get(position).setVolReview("2");
                                        db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                        break;
                                    case 2:
                                        pom.get(position).setVolReview("3");
                                        db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                        break;
                                    case 3:
                                        pom.get(position).setVolReview("4");
                                        db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                        break;
                                    case 4:
                                        pom.get(position).setVolReview("5");
                                        db.child("tasks").child(pom.get(position).getTask()).setValue(pom.get(position));
                                        break;
                                }
                            }
        });
            }else {
                builder1.setMessage(" Description: " + pom.get(position).getDescription()
                        + "\n Location: " + pom.get(position).getLocation()
                        + "\n Frequency: " + pom.get(position).getFrequency()
                        + "\n Status: " + pom.get(position).getState()
                        + "\n Adult: " + pom.get(position).getAdlName()
                        + "\n Number: " + pom.get(position).getAdlNum()
                        + "\n Email: " + pom.get(position).getAdlMail()
                        + "\n Review: " + pom.get(position).getVolReview());
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Okay",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
            }
        }

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public boolean onTouchEvent(MotionEvent touchEvent){
        switch(touchEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = touchEvent.getX();
                y1 = touchEvent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchEvent.getX();
                y2 = touchEvent.getY();
                if(x1<x2){
                    Intent i = new Intent(VolunteerActivity.this, InfoActivity.class);
                    startActivity(i);
                }
                break;
        }
        return false;
    }

}